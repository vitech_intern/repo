package Polindrom;

import org.junit.Assert;
import org.junit.Test;

public class PolindromTest {

    @Test
    public void isPolindromeTest(){
        Assert.assertTrue(Polindrom.isPolindrome("nazan"));
        Assert.assertFalse(Polindrom.isPolindrome("nnazan"));
        Assert.assertFalse(Polindrom.isPolindrome("n"));
        Assert.assertFalse(Polindrom.isPolindrome(" nazan"));
        Assert.assertFalse(Polindrom.isPolindrome("nazan "));
        Assert.assertFalse(Polindrom.isPolindrome(" na z an "));
        Assert.assertTrue(Polindrom.isPolindrome("naan"));
        Assert.assertTrue(Polindrom.isPolindrome("укку"));
        Assert.assertFalse(Polindrom.isPolindrome(""));
    }

}