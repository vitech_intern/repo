package Employee;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class EmployeeTest {

    public SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    private Employee e, e1, e2, e3, e4, e5, e6, e7;
    List<Employee> unsorted,sortedByBirthday, sortedByFirstLastName, sortedById;

    public void createList() {

        e = new Employee(12345678, "Іван", "Дорн", toDate("1988-10-17"));
        e1 = new Employee(12344348, "Андрій", "Кузьменко", toDate("1968-08-17"));
        e2 = new Employee(44766776, "Василь", "Вірастюк", toDate("1974-04-22"));
        e3 = new Employee(42312133, "Анна", "Заячківська", toDate("1991-12-12"));
        e4 = new Employee(21321332, "Юрій", "Филюк", toDate("1982-10-28"));

        e5 = new Employee(34234463, "Олександр", "Усик", toDate("1987-01-17"));
        e6 = new Employee(43346785, "Руслана", "Лижичко", toDate("1973-05-24"));
        e7 = new Employee(23555678, "Юлія", "Саніна", toDate("1990-10-11"));

        unsorted = new ArrayList<Employee>(Arrays.asList(e, e1, e2, e3, e4));

        sortedByFirstLastName = new ArrayList<Employee>(Arrays.asList(e1, e3, e2, e4, e));
        sortedById = new ArrayList<Employee>(Arrays.asList(e1, e, e4, e3, e2));
        sortedByBirthday = new ArrayList<Employee>(Arrays.asList(e1, e2, e4, e, e3));

    }

    @Before
    public void init(){
        createList();
    }

    private Date toDate(String dateStr) {
        try {
            return FORMATTER.parse(dateStr);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);

        }
    }

    @Test
    public void testEmployeesComparatorByFirstLastName() {
        assertEquals(sortedByFirstLastName, EmployeeUtils.sortByFirstLastName(unsorted));
    }

    @Test
    public void testEmployeesComparatorById() {
        assertEquals(sortedById, EmployeeUtils.sortById(unsorted));
    }

    @Test
    public void testEmployeesComparatorByBirthday() {
        assertEquals(sortedByBirthday, EmployeeUtils.sortByBirthDate(unsorted));
    }

    public void assertEquals(List<Employee> expexted, List<Employee> sortedByTest) {
        Assert.assertEquals(expexted.size(), sortedByTest.size());
        for (int i = 0; i < expexted.size(); i++) {
            Assert.assertEquals(expexted.get(i), sortedByTest.get(i));
        }
    }
}
