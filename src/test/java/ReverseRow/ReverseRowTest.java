package ReverseRow;

import org.junit.Assert;
import org.junit.Test;

public class ReverseRowTest {

    @Test
    public void testReverseText() {
        Assert.assertEquals(ReverseRow.reverseText("123"), "321");
        Assert.assertEquals(ReverseRow.reverseText("Nazar"), "razaN");
        Assert.assertEquals(ReverseRow.reverseText("Naza r"), "r azaN");
        Assert.assertEquals(ReverseRow.reverseText("Naza' r"), "r 'azaN");
        Assert.assertEquals(ReverseRow.reverseText(""), "");
        Assert.assertNotEquals(ReverseRow.reverseText("Naza r"), "razaN");
    }
}
