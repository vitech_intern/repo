package MinMaxAvg;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MinMaxAvgTest {

    List<Double> inputList = new ArrayList<Double>(Arrays.asList(1.0, 3.0, 5.0));
    List<Double> emptyList = new ArrayList<Double>();
    List<Double> oneElement = new ArrayList<Double>(Collections.singletonList(1.0));


    List<Double> result = new ArrayList<Double>(Arrays.asList(1.0, 5.0, 3.0));
    List<Double> result2 = new ArrayList<Double>(Arrays.asList(1.0, 5.0, 2.0));
    List<Double> resultOneElement = new ArrayList<Double>(Arrays.asList(1.0, 1.0, 1.0));


    @Test
    public void TestMinMaxAvg(){
       Assert.assertEquals(MinMax.MinMaxAvg.getMinMaxAvg(inputList),result);
       Assert.assertNotEquals(MinMax.MinMaxAvg.getMinMaxAvg(inputList),result2);
       Assert.assertEquals(MinMax.MinMaxAvg.getMinMaxAvg(emptyList),emptyList);
       Assert.assertEquals(MinMax.MinMaxAvg.getMinMaxAvg(oneElement),resultOneElement);
       Assert.assertNotEquals(MinMax.MinMaxAvg.getMinMaxAvg(oneElement),emptyList);
    }

}
