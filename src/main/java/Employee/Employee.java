package Employee;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;

import javax.xml.bind.annotation.XmlElement;
import java.text.SimpleDateFormat;
import java.util.Date;


//this class is a copy of original Employee.class but refractored with google.Guava
public class Employee implements Comparable<Employee> {

    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

    @JsonProperty("Id")
    @XmlElement(name = "Id2e1e21e212e1")
    private Integer id;


    @JsonProperty("firstName")
    @XmlElement(name = "firstName")
    private String firstName;

    @JsonProperty("lastName")
    @XmlElement(name = "lastName")
    private String lastName;

    @JsonProperty("dateOfBirth")
    @XmlElement(name = "dateOfBirth")
    private Date dateOfBirth;

    public Employee(Integer id, String firstName, String lastName, Date dateOfBirth) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = new Date(dateOfBirth.getTime());
    }

    public Employee() {
    }

    public final Integer getId() {
        return this.id;
    }

    public final String getFirstName() {
        return this.firstName;
    }

    public final String getLastName() {
        return this.lastName;
    }


    public final Date getDateOfBirth() {

        return new Date(dateOfBirth.getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equal(id, employee.id) &&
                Objects.equal(firstName, employee.firstName) &&
                Objects.equal(lastName, employee.lastName) &&
                Objects.equal(dateOfBirth, employee.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, firstName, lastName, dateOfBirth);
    }

    @Override
    public int compareTo(Employee o) {
        return ComparisonChain.start()
                .compare(lastName, o.getLastName())
                .compare(firstName, o.getFirstName())
                .compare(this.getDateOfBirth(), o.getDateOfBirth())
                .compare(id,o.getId())
                .result();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("firstName", firstName)
                .add("lastName", lastName)
                .add("dateOfBirth", this.getDateOfBirth())
                .toString();
    }
}
