package Employee.comparators;

import Employee.Employee;

import java.util.Comparator;

public class ComparatorById implements Comparator<Employee> {

    public int compare(Employee o1, Employee o2) {

        Integer firstId = o1.getId();
        Integer secondId = o2.getId();

        if (firstId == null || secondId == null) {
            return -1;
        } else {
            return firstId.compareTo(secondId);
        }
    }
}
