package Employee.comparators;

import Employee.Employee;

import java.util.Comparator;

public class ComparatorByFirstLastName implements Comparator<Employee> {

    public int compare(Employee o1, Employee o2) {

        String firstName1 = o1.getFirstName();
        String firstName2 = o2.getFirstName();
        String lastName1 = o1.getLastName();
        String lastName2 = o2.getLastName();


        if (firstName1 != null && firstName2 != null) {

            if (firstName1.compareToIgnoreCase(firstName2) == 0) {

                if (lastName1 != null && lastName2 != null) {

                    return lastName1.compareToIgnoreCase(lastName2);

                } else {
                    return -1;
                }

            } else {
                return firstName1.compareToIgnoreCase(firstName2);
            }

        } else if (lastName1 != null && lastName2 != null) {

            return lastName1.compareToIgnoreCase(lastName2);

        } else {
            return -1;
        }
    }
}
