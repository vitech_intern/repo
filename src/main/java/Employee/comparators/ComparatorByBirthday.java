package Employee.comparators;

import Employee.Employee;

import java.util.Comparator;
import java.util.Date;

public class ComparatorByBirthday implements Comparator<Employee> {

    public int compare(Employee o1, Employee o2) {

        Date dateOfBirth1 = o1.getDateOfBirth();
        Date dateOfBirth2 = o2.getDateOfBirth();


        if (dateOfBirth1 != null && dateOfBirth2 != null) {
            return dateOfBirth1.compareTo(dateOfBirth2);

        } else {
            return -1;
        }

    }
}
