package Employee.Employee_old;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.text.SimpleDateFormat;
import java.util.Date;

@XmlRootElement
public class Employee implements Comparable<Employee>{

    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

    @JsonProperty("Idwdwqdqd")
    @XmlElement(name = "Id2e1e21e212e1")
    private Integer id;


    @JsonProperty("firstName")
    @XmlElement(name = "firstName")
    private String firstName;

    @JsonProperty("lastName")
    @XmlElement(name = "lastName")
    private String lastName;

    @JsonProperty("dateOfBirth")
    @XmlElement(name = "dateOfBirth")
    private Date dateOfBirth;


    public Employee(Integer id, String firstName, String lastName, Date dateOfBirth) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = new Date(dateOfBirth.getTime());
    }

    public Employee() {
    }


    public final Integer getId() {
        return this.id;
    }

    public final String getFirstName() {
        return this.firstName;
    }

    public final String getLastName() {
        return this.lastName;
    }


    public final Date getDateOfBirth() {

        return new Date(dateOfBirth.getTime());
    }

    @Override
    public String toString() {
        StringBuilder toString = new StringBuilder();
        toString.append(this.id).append(" ").append(this.firstName).append(" ").append(this.lastName).append(" ").append((FORMATTER.format(this.dateOfBirth)));
        return toString.toString();
    }

    @Override
    public int compareTo(Employee o) {
        String firstName1 = this.lastName;
        String lastName1 =this.lastName;

        String firstName2 = o.getFirstName();
        String lastName2 = o.getLastName();

        if (lastName1 != null && lastName2 != null) {
            if (lastName1.compareToIgnoreCase(lastName2) == 0) {
                if (firstName1 != null && firstName2 != null) {
                    return firstName1.compareToIgnoreCase(firstName1);
                } else {
                    return -1;
                }
            } else {
                return lastName1.compareToIgnoreCase(lastName2);
            }
        } else if (firstName1 != null && firstName2 != null) {
            return firstName1.compareToIgnoreCase(firstName2);
        } else {
            return -1;
        }
    }
}
