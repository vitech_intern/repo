package Employee;

import Employee.comparators.ComparatorByBirthday;
import Employee.comparators.ComparatorByFirstLastName;
import Employee.comparators.ComparatorById;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.apache.commons.collections4.Predicate;
import org.apache.log4j.Logger;
import Employee.predicates.*;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class EmployeeUtils {

    private static final Logger LOGGER = Logger.getLogger(MainController.class);

    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

    private static int id;
    private static String firstName;
    private static String lastName;
    private static Date dateOfBirth;
    private int startpos;
    private int endpos;

    public static List<Employee> sortById(List<Employee> employees){
        Collections.sort(employees,new ComparatorById());
        return employees;
    }

    public static List<Employee> sortByFirstLastName(List<Employee> employees){
        Collections.sort(employees,new ComparatorByFirstLastName());
        return employees;
    }

    public static List<Employee> sortByBirthDate(List<Employee> employees){
        Collections.sort(employees,new ComparatorByBirthday());
        return employees;
    }

    public static void toJSON(List<Employee> employees) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        //mapper.writeValue(new File("employees.json"), employees);
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File("employees.json"), employees);
        LOGGER.info("json created!");
    }

    public static List<Employee> toJavaObject() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        List<Employee> list = mapper.readValue(new File("employees.json"), new TypeReference<List<Employee>>() {
        });
        //return mapper.readValue(new File("employees.json"), Employee.class);
        return list;
    }

    public static void toXML(List<Employee> employees) throws IOException {
        XmlMapper mapper = new XmlMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.writeValue(new File("employees.xml"), employees);
        LOGGER.info("xml created!");
    }

    public static List<Employee> fromXMLtoJavaObject() throws IOException {
        XmlMapper mapper = new XmlMapper();
        List<Employee> list = mapper.readValue(new File("employees.xml"), new TypeReference<List<Employee>>() {
        });
        return list;
    }

    public static void iterateAndPrintPrettyJSON(List<Employee> employees) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString;
        for (Employee e : employees) {
            try {
                jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(e);
                LOGGER.info(jsonInString);
            } catch (JsonProcessingException e1) {
                e1.printStackTrace();
            }
        }
    }


    public static void iterateAndPrint(List<Employee> employees) {
        for (Employee e : employees) {
            LOGGER.info(String.format("%-12d%-12s%-15s%-10s", e.getId(), e.getFirstName(), e.getLastName(), FORMATTER.format(e.getDateOfBirth())));
        }
    }

    public static final Predicate OLDERTHAN = new OlderThan("1980-01-25");

    public List<Employee> intersection(List<Employee> e1, List<Employee> e2) {

        List<Employee> intersection = new ArrayList<Employee>();

        for (Employee e : e2) {
            if (e1.contains(e)) {
                intersection.add(e);
            }
        }
        return intersection;
    }

    public List<Employee> difference(List<Employee> e1, List<Employee> e2) {

        List<Employee> difference = new ArrayList<Employee>();

        for (Employee e : e2) {
            if (!e1.contains(e)) {
                difference.add(e);
            }
        }
        for (Employee ee : e1) {
            if (!e2.contains(ee)) {
                difference.add(ee);
            }
        }
        return difference;
    }

    public List<Employee> stringsToObjects(List<String> EmployeesAsStrings) throws ParseException {

        List<Employee> employees = new ArrayList<Employee>();

        for (String s : EmployeesAsStrings) {
            if (!s.isEmpty()) {

                id = 0;
                firstName = "";
                lastName = "";
                startpos = 0;

                //getting id
                endpos = s.indexOf(",", startpos);
                id = Integer.valueOf(s.substring(startpos, endpos));

                //getting firstName
                startpos = ++endpos;
                endpos = s.indexOf(",", startpos);
                firstName = s.substring(startpos, endpos);

                //getting lastName
                startpos = ++endpos;
                endpos = s.indexOf(",", startpos);
                lastName = s.substring(startpos, endpos);

                //getting birthday
                startpos = ++endpos;
                endpos = s.length();
                String date = s.substring(startpos, endpos);
                dateOfBirth = FORMATTER.parse(date);

                //creating Employee object & adding to employees list
                Employee emp = new Employee(id, firstName, lastName, dateOfBirth);
                employees.add(emp);
            }
        }
        return employees;
    }
}
