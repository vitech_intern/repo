package Employee;

import Employee.comparators.ComparatorByFirstLastName;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MainController {

    static Logger LOGGER = Logger.getLogger(MainController.class);
    //final static SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

    public static void main(String args[]) throws ParseException, IOException {

        String workdir = "/Users/demo/Desktop/Vitech_Inern/src/main/java/Employee";
        File group = new File(workdir, "Employees.txt");

        List<String> EmployeesAsStrings = new ArrayList<String>();
        String temp = "";

        BufferedReader br = new BufferedReader(new FileReader(group));

        while ((temp = br.readLine()) != null) {
            EmployeesAsStrings.add(temp);
        }
        br.close();

        EmployeeUtils EmployeeUtils = new EmployeeUtils();

        List<Employee> employees = EmployeeUtils.stringsToObjects(EmployeesAsStrings);
        EmployeeUtils.iterateAndPrint(employees);

        LOGGER.info("");

        List<Employee> soretedById = EmployeeUtils.sortById(employees);
        LOGGER.info("Sorted by Id");
        EmployeeUtils.iterateAndPrint(soretedById);

        LOGGER.info("");

        List<Employee> soretedByFirstLastName = EmployeeUtils.sortByFirstLastName(employees);
        LOGGER.info("Sorted by FirstLastName");
        EmployeeUtils.iterateAndPrint(soretedByFirstLastName);

        LOGGER.info("");

        List<Employee> soretedByByBirthdate = EmployeeUtils.sortByBirthDate(employees);
        LOGGER.info("Sorted by BirthDate");
        EmployeeUtils.iterateAndPrint(employees);

        LOGGER.info("");

        //filtering & printing list of employees by using ApacheCommonsCollections
        List<Employee> olderThan = new ArrayList<Employee>(employees);
        CollectionUtils.filter(olderThan, EmployeeUtils.OLDERTHAN);
        LOGGER.info("those who were born after 25 Jan 1980");
        EmployeeUtils.iterateAndPrint(olderThan);

        LOGGER.info("");

        //printing intersection of filtered & original lists sorted by names
        List<Employee> intersection = EmployeeUtils.intersection(employees, olderThan);
        intersection.sort(new ComparatorByFirstLastName());
        LOGGER.info("Intersection of all & those who were born after 25 Jan 1980");
        EmployeeUtils.iterateAndPrint(intersection);

        LOGGER.info("");

        //printing difference of filtered & original lists sorted by names
        List<Employee> difference = EmployeeUtils.difference(employees, olderThan);
        difference = EmployeeUtils.sortByFirstLastName(difference);

        LOGGER.info("Difference of all & those who were born after 25 Jan 1980");
        EmployeeUtils.iterateAndPrint(difference);

        EmployeeUtils.toJSON(difference);
        EmployeeUtils.iterateAndPrintPrettyJSON(EmployeeUtils.toJavaObject());


        EmployeeUtils.toXML(employees);
        EmployeeUtils.iterateAndPrint(EmployeeUtils.fromXMLtoJavaObject());

        LOGGER.info("");
        LOGGER.info("sorted by default");
        LOGGER.info("");

        Collections.sort(employees);
        EmployeeUtils.iterateAndPrint(employees);

    }
}
