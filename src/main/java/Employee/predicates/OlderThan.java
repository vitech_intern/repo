package Employee.predicates;

import Employee.Employee;
import org.apache.commons.collections4.Predicate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OlderThan implements Predicate {

    private final static SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    private Date filterDate;

    public OlderThan(final String dateAsStr) {
        try {
            this.filterDate = FORMATTER.parse(dateAsStr);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Not correct date setup !!!");
        }
    }

    @Override
    public boolean evaluate(Object o) {

        return ((Employee) o).getDateOfBirth().after(this.filterDate);

    }
}
