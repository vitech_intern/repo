package Polindrom;

import org.apache.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polindrom {

    static Logger logger = Logger.getLogger(Polindrom.class);

    public static void main(String args[]) {
        start();
    }

    public static boolean isPolindrome(String word) {
        word = word.toLowerCase();
        int len = word.length();

        if (len < 3) {
            logger.info("NO! Word isn't polindrome");
            return false;
        }

        if (isWord(word)) {
            for (int i = 0; i <= len / 2; i++) {

                if (word.charAt(i) != word.charAt(len - i - 1)) {
                    logger.info("NO! Word isn't polindrome");
                    return false;
                }

            }
            logger.info("YES! Word is a polindrome");
            return true;
        }
        logger.info("NO! Word isn't polindrome");
        return false;
    }

    public static boolean isWord(String testString) {
        Pattern p = Pattern.compile("^[a-zа-яїґ]+$");
        Matcher m = p.matcher(testString);
        return m.matches();
    }

    public static void start() {
        Scanner inputWord = new Scanner(System.in);

        logger.info("Please enter the word: ");
        String word = inputWord.nextLine();

        while (!word.equalsIgnoreCase("exit")) {
            isPolindrome(word);
            logger.info("New word please: ");
            word = inputWord.nextLine();
        }
        logger.info("Good bye !");
    }
}
