package ReverseRow;

import org.apache.log4j.Logger;

import java.util.Scanner;

public class ReverseRow {

    static Logger logger = Logger.getLogger(ReverseRow.class);
    Scanner inputText = new Scanner(System.in);

    public static void main(String args[]) {

        ReverseRow rw = new ReverseRow();
        rw.startTextInput();
    }

    public static String reverseText(String text) {
        int len = text.length();
        StringBuilder reversedText = new StringBuilder();
        for (int i = len - 1; i >= 0; --i) {
            reversedText.append(text.charAt(i));
        }
        return reversedText.toString();
    }

    public void startTextInput() {

        logger.info("Please enter the word: ");
        String text = inputText.nextLine();

        if (!text.equalsIgnoreCase("exit321")) {
            reverseText(text);
            logger.info("Your text is: " + text);
            logger.info("Reversed text: " + reverseText(text));
            startTextInput();
        } else {
            logger.info("Good bye !");
            return;
        }
    }
}
