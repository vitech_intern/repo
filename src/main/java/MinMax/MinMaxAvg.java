package MinMax;

import org.apache.log4j.Logger;

import java.util.*;

public class MinMaxAvg {

    static Logger LOGGER = Logger.getLogger(MinMaxAvg.class);

    Scanner INPUT = new Scanner(System.in);

    List<Double> allNumbers = new ArrayList<Double>();

    public static List<Double> getMinMaxAvg(List<Double> list) {

        List<Double> result = new ArrayList<Double>();

        int size = list.size();
        if(size != 0){
            Double firstElement = list.get(0);
            double min = firstElement;
            double max = firstElement;
            double sum = firstElement;

            for (int i = 1; i < size; i++) {

                if (list.get(i) > max) {
                    max = list.get(i);
                }
                if (list.get(i) < min) {
                    min = list.get(i);
                }
                sum = sum + list.get(i);

            }
            double avg = sum / size;
            result.add(min);
            result.add(max);
            result.add(avg);

            return result;
        }else{
            return list;
        }

    }

    public List<Double> getArrayInput() {

        allNumbers.clear();
        LOGGER.info("Enter a number: ");
        String stringInput = INPUT.nextLine();

        String[] strings = stringInput.split(" ");

        if (!stringInput.equalsIgnoreCase("stop")) {
            for (String s : strings) {
                if (s.matches("-?\\d+(\\.\\d+)?")) {
                    allNumbers.add(Double.parseDouble(s));
                }
            }
        } else {
            getArrayInput();
        }

        if (allNumbers.size() == 0) {
            getArrayInput();
        }
        List<Double> result = getMinMaxAvg(allNumbers);
        LOGGER.info(formatResult(result));

        getArrayInput();
        return allNumbers;
    }

    private String formatResult(List<Double> result) {
        return "min = " + result.get(0)
                + "; max = " + result.get(1) + "; avg " + result.get(2);
    }

    public List<Double> getStepByStepInput() {

        try {
            LOGGER.info("Enter a number: ");
            String element = INPUT.next();
            if (!element.equalsIgnoreCase("stop")) {
                allNumbers.add(Double.parseDouble(element));
                getMinMaxAvg(allNumbers);
                getStepByStepInput();
            } else {
                chooseVariant();
            }
        } catch (NumberFormatException e) {
            LOGGER.info("Not a number. ");
            getStepByStepInput();
        }
        return allNumbers;
    }

    public void chooseVariant() {

        LOGGER.info("Please choose Enter type: ");
        LOGGER.info("by array: 0");
        LOGGER.info("step by step: 1");

        try {
            String element = INPUT.next();
            if (element.equalsIgnoreCase("stop")) {
                return;
            }
            switch (Integer.valueOf(element)) {
                case 0:
                    getArrayInput();
                    break;
                case 1:
                    getStepByStepInput();
                    break;
                default:
                    LOGGER.info("Choose correct number ");
                    chooseVariant();
            }

        } catch (NumberFormatException e) {
            LOGGER.info("Not a number. Choose correct number ");
            chooseVariant();
        }

    }
}
